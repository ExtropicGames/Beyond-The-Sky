#ifndef MAIN_STATE_H
#define MAIN_STATE_H

#include <list>

#ifdef WIN32
#include <SDL.h>
#include <SDL_mixer.h>
#include <SDL_rotozoom.h>
#else
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include <SDL/SDL_rotozoom.h>
#endif

#include "StateMachine.h"
#include "Actor.h"
#include "Map.h"
#include "PlanetFactory.h"

const int SCREEN_WIDTH = 300*4;
const int SCREEN_HEIGHT = 175*4;
const int FRAMES_PER_SECOND = 60;

class MainState : public State {
public:
    int id() { return 1; }

    void start();
    void finish();
    void update();
    void render();
private:
    SDL_Surface* screen;
    SDL_Surface* video_surface;

    SDL_Surface* sky;
    std::shared_ptr<GameData> game;
    Camera cam;
};

#endif
