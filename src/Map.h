#ifndef MAP_H
#define MAP_H

#include <string>
#include <utility>

#ifdef WIN32
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif

#include "Resources/Bitmask.h"
#include "GameData.h"

// map related constants
const int MAP_HEIGHT = 512;
const int MAP_WIDTH = 512;

class GameData;

struct MapSlice {
    SDL_Surface* foreground;
    SDL_Surface* background;
    std::shared_ptr<Bitmask> collision;
};

class Map {
public:
    Map(int planet_id, int starting_location);

	void update();
	void renderBackground(SDL_Surface* screen, std::shared_ptr<GameData> game);
	void renderForeground(SDL_Surface* screen, std::shared_ptr<GameData> game);

	bool collides(SDL_Rect area);
    void scrollLeft();
    void scrollRight();
    void moveTo(int planet_id, int location);
    
private:
    int planet_id;
    int location;
    
    std::shared_ptr<MapSlice> left;
    std::shared_ptr<MapSlice> center;
    std::shared_ptr<MapSlice> right;
};

#endif