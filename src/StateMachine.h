#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include <memory>
#include <map>

#include <cassert>

// 0 is the end state

class StateMachine;

class State {
public:
    friend class StateMachine;

    virtual int id() = 0;
    virtual void update() {}
    virtual void render() {}
    virtual void start() {}
    virtual void finish() {}
protected:
    StateMachine* sm;
};

class QuitState : public State {
public:
    int id() { return 0; }
};

class StateMachine {
public:
    StateMachine() {
        current = std::shared_ptr<State>();
    }

    void add(std::shared_ptr<State> state) {
        states[state->id()] = state;
        state->sm = this;
    }
    void start(int id) {
        current = states[id];
        assert(current);
        current->start();
    }
    void transition(int id) {
        current->finish();
        current = states[id];
        assert(current);
        current->start();
    }

    void update() {
        current->update();
    }

    void render() {
        current->render();
    }

    bool quit() {
        if (current->id() == 0) {
            return true;
        }
        return false;
    }
private:
    std::map<int, std::shared_ptr<State>> states;
    std::shared_ptr<State> current;
};

#endif
