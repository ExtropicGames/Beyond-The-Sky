#include <iostream>

#include <cassert>

#ifdef WIN32
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#else
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_mixer.h>
#endif

#include "StateMachine.h"
#include "MainState.h"

using namespace std;

int main(int argc, char** argv) {

    // Set up SDL core
    atexit(SDL_Quit);
    if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
        cout << "Could not init SDL." << endl;
        exit(1);
    }

    // Set up SDL mixer
    atexit(Mix_CloseAudio);
    // Set up SDL_mixer
    if (Mix_OpenAudio(22050, AUDIO_S16, 2, 4096)) {
        cout << "Could not init SDL_mixer." << endl;
        exit(1);
    }
    
    // Get some information about the environment before we start initializing things.
    const SDL_VideoInfo* info = SDL_GetVideoInfo();
    
    // Set up OpenGL attributes
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8); // 8 bits of red
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8); // 8 bits of green
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8); // 8 bits of green
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16); // 16 bits for the depth buffer
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); // Turn on double buffering
    
    SDL_Surface* screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, info->vfmt->BitsPerPixel, SDL_HWSURFACE|SDL_DOUBLEBUF);
    //SDL_Surface* screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, info->vfmt->BitsPerPixel, SDL_OPENGL);

    assert(screen);

    SDL_WM_SetCaption("SDLPlatformer", NULL);

    SDL_EnableKeyRepeat(50, 50);

    StateMachine sm;
    sm.add(make_shared<QuitState>());
    sm.add(make_shared<MainState>());

    sm.start(1);

    while (!sm.quit()) {
        sm.update();
        sm.render();
    }

    return 0;
}
