#ifndef SDLFUNC_H
#define SDLFUNC_H

#ifdef WIN32
#include <SDL.h>
#include <SDL_image.h>
#else
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#endif

SDL_Surface* loadImage(const char* filename);
void paletteSwap(SDL_Surface* surface, SDL_Color* c1, SDL_Color* c2);

Uint32 getpixel(SDL_Surface *surface, int x, int y);

#endif