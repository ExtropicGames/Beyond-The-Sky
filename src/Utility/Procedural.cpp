#include <cmath>

#include "Procedural.h"

//! id selects which noise function to use. Each
//! noise function uses a different set of prime numbers
//! to generate noise.
double Noise(int x, int id) {
    long p1;
    long p2;
    long p3;
    if (id == 0) {
        p1 = 15731;
        p2 = 789221;
        p3 = 1376312579;
    } else if (id == 1) {
        p1 = 17939;
        p2 = 789527;
        p3 = 1376312627;
    } else {
        // select default set of prime numbers
    }
    x = (x << 13) ^ x;
    return ( 1.0 - ( (x * (x * x * p1 + p2) + p3) & 0x7fffffff) / 1073741824.0);
}

double SmoothedNoise(double x, int id) {
    return Noise(x, id)/2 + Noise(x-1, id)/4 + Noise(x+1, id)/4;
}

double LinearInterpolate(double a, double b, double x) {
    return a * (1 - x) + (b * x);
}

double CosineInterpolate(double a, double b, double x) {
    const double ft = x * 3.141592653;
    const double f = (1 - cos(ft)) * 0.5f;
    
    return a * (1 - f) + b * f;
}

double InterpolatedNoise(double x, int id) {
    const long intx = (long) x;
    const double fractx = x - intx;
    
    const double v1 = SmoothedNoise(intx, id);
    const double v2 = SmoothedNoise(intx + 1, id);
    
    return LinearInterpolate(v1, v2, fractx);
}

double PerlinNoise(double x) {
    double total = 0 ;
    const double p = 1;
    const long n = 10;
    
    for (int i = 0; i < n; i++) {
        const long frequency = pow(2, i);
        const long amplitude = pow(p, i);
        
        total = total + InterpolatedNoise(x * frequency, i) * amplitude;
    }
    
    return total;
}