#ifndef PROCEDURAL_H
#define PROCEDURAL_H

#include <cmath>

// Interpolation functions
double LinearInterpolate(double a, double b, double x);
double CosineInterpolate(double a, double b, double x);

// Noise functions
double Noise(int x, int id);
double SmoothedNoise(double x, int id);
double InterpolatedNoise(double x, int id);
double PerlinNoise(double x);

#endif