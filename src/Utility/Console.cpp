#include "Console.h"

#include <iostream>

using namespace std;

void Console::RegisterVariable(string name, int* integer) {
    //variable_integers.put(name, integer);
}

void Console::EnterMethod(string method_name) {
    cout << method_name << " -->" << endl;
}

void Console::ExitMethod(string method_name) {
    cout << method_name << " <--" << endl;
}