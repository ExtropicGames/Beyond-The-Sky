#ifndef CONSOLE_H
#define CONSOLE_H

#include <string>
#include <map>

class Console {
public:
    static void RegisterVariable(std::string name, int* integer);
    static void EnterMethod(std::string method_name);
    static void ExitMethod(std::string method_name);
private:
    static std::map<std::string, int*> variable_integers;
};

#endif