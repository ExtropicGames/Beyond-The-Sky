#include "Bitmask.h"

#include <iostream>

#include "Utility/sdlfunc.h"

using namespace std;

// TODO: speed everything up by using ints or chars for the bitmask and comparing relevant values to zero

Bitmask::Bitmask(SDL_Surface* surface) {
    width = surface->w;
    height = surface->h;
    mask = new bool[width * height];
    for (int i = 0; i < width; i++) {
        for (int m = 0; m < height; m++) {
            Uint32 pixel = getpixel(surface, i, m);
            if (pixel == 0) {
                mask[i + (m * width)] = false;
            } else {
                mask[i + (m * width)] = true;
            }
        }
    }
}

Bitmask::Bitmask(int _width, int _height) {
    width = _width;
    height = _height;
    mask = new bool[width * height];
}

Bitmask::~Bitmask() {
    delete [] mask;
}

bool Bitmask::collides(int x, int y) {
    return mask[x + (y * width)];
}

bool Bitmask::collides(int x, int y, int w, int h) {
    for (int i = 0; i < width; i++) {
        for (int m = 0; m < height; m++) {
            if (mask[i + (m * width)] && i >= x && i <= w && m >= y && m <= h) {
                return true;
            }
        }
    }
    
    return false;
}

bool Bitmask::collides(Bitmask &other) {
    // TODO: handle bitmasks that don't completely overlap
    if (width * height > other.width * other.height) {
        for (int i = 0; i < width; i++) {
            for (int m = 0; m < height; m++) {
                if (mask[i + (m * width)] && other.mask[i + (m * width)]) {
                    return true;
                }
            }
        }
    } else {
        for (int i = 0; i < other.width; i++) {
            for (int m = 0; m < other.height; m++) {
                if (mask[i + (m * width)] && other.mask[i + (m * width)]) {
                    return true;
                }
            }
        }
    }
    return false;
}