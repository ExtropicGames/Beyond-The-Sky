#ifndef ANIMATION_H
#define ANIMATION_H

#include <string>

#ifdef WIN32
#include <SDL.h>
#include <SDL_rotozoom.h>
#else
#include <SDL/SDL.h>
#include <SDL/SDL_rotozoom.h>
#endif

enum AnimAction {
    STANDING,
    MOVING,
    JUMPING,
    FALLING
};

class Animation {
public:
    Animation(std::string name);
    
    void render(SDL_Surface* surface, Sint16 x, Sint16 y, AnimAction action, int frame, bool flipped);
    
private:
    SDL_Surface* spritesheet;
    Uint16 sprite_width;
    Uint16 sprite_height;
    Uint16 spritesheet_width;
};

#endif