#include "Animation.h"

#include "Utility/sdlfunc.h"

using namespace std;

Animation::Animation(string name) {
    spritesheet = loadImage(("data/characters/" + name + ".png").c_str());
    sprite_width = 8;
    sprite_height = 8;
    spritesheet_width = 8 * 3;
}

void Animation::render(SDL_Surface* surface, Sint16 x, Sint16 y, AnimAction action, int frame, bool flipped) {

    SDL_Rect where = {x, y, sprite_width, sprite_height};
    SDL_Rect clip;
    
    // get the animation from the spritesheet
    if (action == STANDING) {
        clip = {0, 0, sprite_width, sprite_height};
    } else if (action == MOVING) {
        clip = {(Sint16) (sprite_width * (frame + 1)), 0, sprite_width, sprite_height};
    }
    
    // render it to the surface
    if (flipped) {
        clip.x = (spritesheet_width - sprite_width) - clip.x;
        SDL_Surface* flip = zoomSurface(spritesheet, -1, 1, 0);
        SDL_BlitSurface(flip, &clip, surface, &where);
    } else {
        SDL_BlitSurface(spritesheet, &clip, surface, &where);
    }
}