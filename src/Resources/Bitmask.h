#ifndef BITMASK_H
#define BITMASK_H

#ifdef WIN32
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif

class Bitmask {
public:
    Bitmask(SDL_Surface* surface);
    Bitmask(int _width, int _height);
    ~Bitmask();
    
    bool collides(int x, int y);
    bool collides(int x, int y, int w, int h);
    bool collides(Bitmask &other);
private:
    Uint32 width;
    Uint32 height;
    bool* mask;
};

#endif