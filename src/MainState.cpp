#include "MainState.h"

#include <fstream>
#include <iostream>

#include "Utility/sdlfunc.h"

using namespace std;

void MainState::start() {

    video_surface = SDL_GetVideoSurface();
    const SDL_PixelFormat& fmt = *(video_surface->format);
    screen = SDL_CreateRGBSurface(SDL_HWSURFACE, 300, 175, fmt.BitsPerPixel, fmt.Rmask, fmt.Gmask, fmt.Bmask, fmt.Amask);

    game.reset(new GameData());

    // load images
    sky = loadImage("data/sky.png");
    game->map.reset(new Map(0, 0));
    game->env = PlanetFactory::generatePlanetEnvironment(0);
    
    game->player.reset(new Actor("astronaut"));
    game->player->position_y = 0;
    while (!game->player->onSurface(game)) {
        game->player->position_y++;
    }
    
    game->camera.reset(new Camera());

    // read saved data
    // ifstream save_file("saves/file1.sav");
    // if (save_file.good()) {
    //     save_file.read((char*) &data, sizeof(int));
    // } else {
    //     data = default;
    // }
    // save_file.close();

    // start playing music
    // Mix_Music* music = Mix_LoadMUS("data/music/whatever.mp3");
    // Mix_PlayMusic(music, -1);
}

void MainState::finish() {
    // write save data
    // ofstream save_file("saves/whatever.sav");
    // if (save_file.good()) {
    //     save_file.write((char*) &data, sizeof(int));
    // }
    // save_file.close();

    // stop music
    Mix_HaltMusic();

    // free resources
    // Mix_FreeMusic(music);
}

void MainState::update() {

    SDL_Event event;

    // handle events, get the player action
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                sm->transition(0);
                return;
            } else if (event.key.keysym.sym == SDLK_LEFT) {
                game->player->doAction(RUN_LEFT);
            } else if (event.key.keysym.sym == SDLK_RIGHT) {
                game->player->doAction(RUN_RIGHT);
            } else if (event.key.keysym.sym == SDLK_SPACE) {
                game->player->doAction(JUMP);
            }
        } else if (event.type == SDL_QUIT) {
            sm->transition(0);
            return;
        }
    }

    game->player->update(game);
    
    game->env->update();
    
    if ((int) game->player->position_x < 0) {
        game->map->scrollLeft();
        game->player->position_x += MAP_WIDTH;
    } else if ((int) game->player->position_x > MAP_WIDTH) {
        game->map->scrollRight();
        game->player->position_x -= MAP_WIDTH;
    }
    
    game->camera->follow((int) game->player->position_x, (int) game->player->position_y);
}

void MainState::render() {

    SDL_BlitSurface(sky, NULL, screen, NULL);
    
    game->env->render(screen, game);

    // draw the level
    game->map->renderBackground(screen, game);

    game->player->render(screen, game);
    
    game->map->renderForeground(screen, game);
    
    const float scale_factor = 4.0;

    SDL_Surface* final_screen = zoomSurface(screen, scale_factor, scale_factor, false);
    
    SDL_BlitSurface(final_screen, NULL, video_surface, NULL);
    SDL_FreeSurface(final_screen);

    SDL_Flip(video_surface);

    // Regulate frame rate.
    static Uint32 next_frame = 0;
    Uint32 now = SDL_GetTicks();
    if (next_frame <= now) {
        next_frame = now + (1000 / FRAMES_PER_SECOND);
    } else {
        SDL_Delay(next_frame - now);
    }
}
