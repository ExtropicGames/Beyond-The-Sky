#include "PlanetFactory.h"

#include <iostream>
#include <cassert>

#ifdef WIN32
#include <SDL_gfxPrimitives.h>
#else
#include <SDL/SDL_gfxPrimitives.h>
#endif

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Utility/Procedural.h"

using namespace std;

const Uint32 SOLID = 0xFFFFFFFF;
const Uint32 OPEN = 0x0;

shared_ptr<MapSlice> PlanetFactory::generatePlanetMap(int planet_id, int map_location) {
    
    // Create data structures to be used while generating this slice.
    shared_ptr<MapSlice> result(new MapSlice);
    SDL_Surface* video_surface = SDL_GetVideoSurface();
    const SDL_PixelFormat& fmt = *(video_surface->format);
    result->background = SDL_CreateRGBSurface(SDL_HWSURFACE | SDL_SRCALPHA, MAP_HEIGHT, MAP_WIDTH, fmt.BitsPerPixel, fmt.Rmask, fmt.Gmask, fmt.Bmask, 0x000000ff);
    result->foreground = SDL_CreateRGBSurface(SDL_HWSURFACE | SDL_SRCALPHA, MAP_HEIGHT, MAP_WIDTH, fmt.BitsPerPixel, fmt.Rmask, fmt.Gmask, fmt.Bmask, 0x000000ff);
    SDL_Surface* collision = SDL_CreateRGBSurface(SDL_HWSURFACE, MAP_HEIGHT, MAP_WIDTH, fmt.BitsPerPixel, fmt.Rmask, fmt.Gmask, fmt.Bmask, fmt.Amask);

    // Initialize the random seed.
    srand(planet_id);
    
    int palette = rand() % 1;
    
    // Determine the colors to be used on the planet.
    Uint32 ground_color;
    Uint32 spire_color;
    
    if (palette == 0) {
        ground_color = 0xEBCA97FF;
        spire_color = 0xCBAA77FF;
    }
    
    // reticulate splines
    const int num_points = (MAP_WIDTH / 16) + 1;
    int height_points[num_points];
    for (int i = 0; i < num_points; i++) {
        
        height_points[i] = (PerlinNoise((map_location * (num_points - 1)) + i) * 10) + (MAP_HEIGHT / 2);
    }
    int* heightmap = new int[MAP_WIDTH];
    for (int i = 0; i < MAP_WIDTH; i++) {
        int point = (i / 16) + 1;
        
        double lerp_point = (double) (i % 16) / 16.0;
        heightmap[i] = MAP_HEIGHT - CosineInterpolate(height_points[point-1], height_points[point], lerp_point);
    }
    
    /*    
    // draw a spire, maybe
    if ((rand() % 10) == 1) {
        int spire_width = (rand() % (width - 1)) + 1;
        int spire_height = (rand() % (spire_width * 4)) + (spire_width * 4);
        int spire_x = (rand() % (width - spire_width)) + i;
            
        filledTrigonColor(result->background, spire_x, height - 1, spire_x + spire_width, height - 1, spire_x + (spire_width / 2), height - spire_height, spire_color);
    }
    */
    
    // draw the heightmap to the image surfaces
    for (int i = 0; i < MAP_WIDTH; i++) {
        vlineColor(result->background, i, heightmap[i], MAP_HEIGHT, ground_color);
        vlineColor(collision, i, heightmap[i], MAP_HEIGHT, SOLID);
    }
    
    // create a pyramid
    glm::vec3 points[4];
    points[0] = glm::vec3(0.0, 1.0, 0.0) * 20.0f; // top point
    points[1] = glm::vec3(0.5, 0.0, 0.0) * 20.0f;
    points[2] = glm::vec3(-0.5, 0.0, 0.0) * 20.0f;
    points[3] = glm::vec3(0.0, 0.0, 1.0) * 20.0f;
    
    // TODO: translate to the top of the heightmap
    points[0] += glm::vec3();
    
    // TODO: transform to screen coordinates
    
    trigonColor(result->foreground, points[0].x, points[0].y, points[1].x, points[1].y, points[2].x, points[2].y, 0x444444FF);
    
    result->collision.reset(new Bitmask(collision));
    delete [] heightmap;
    SDL_FreeSurface(collision);
    return result;
}

shared_ptr<Environment> PlanetFactory::generatePlanetEnvironment(int planet_id) {
    shared_ptr<Environment> env(new Environment());
    return env;
}

shared_ptr<Planet> PlanetFactory::getPlanetInfo(int planet_id) {
    shared_ptr<Planet> planet(new Planet);
    
    srand(planet_id);
    
    //planet->
    
    return planet;
}