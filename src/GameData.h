#ifndef GAME_DATA_H
#define GAME_DATA_H

#include <memory>

#include "Actor.h"
#include "Environment.h"
#include "Camera.h"

class Actor;
class Map;

class GameData {
public:
    std::shared_ptr<Map> left_map;
    std::shared_ptr<Map> map;
    std::shared_ptr<Map> right_map;
    std::shared_ptr<Environment> env;
    
    std::shared_ptr<Camera> camera;

    // actors
    std::shared_ptr<Actor> player;
};

#endif