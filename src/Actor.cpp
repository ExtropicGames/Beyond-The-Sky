#include "Actor.h"

// c stdlib
#include <cmath>
#include <cassert>

// c++ stdlib
#include <iostream>
#include <Map.h>
#include <memory>

#include "Utility/sdlfunc.h"

using namespace std;

Actor::Actor(string name) {
    anim.reset(new Animation(name));
    position_x = 0;
    position_y = 0;
    velocity_x = 0;
    velocity_y = 0;
}

Actor::~Actor() {
}

//! The doAction function flags which actions the actor wants to take
//! during this frame. The actions will then be activated when update()
//! is called.
void Actor::doAction(Action action) {

    actionQueue.push_back(action);
    
    // make sure velocity doesn't exceed reasonable bounds
    if (velocity_x > 2) {
        velocity_x = 2;
    } else if (velocity_x < -2) {
        velocity_x = -2;
    }
    
    if (velocity_y > 2) {
        velocity_y = 2;
    } else if (velocity_y < -2) {
        velocity_y = -2;
    }
}

void Actor::update(shared_ptr<GameData> game) {
    //SDL_Rect me = {(Sint16) position_x, (Sint16) position_y, 7, 7};
    //cout << "me :" << position_x << ", " << position_y << endl;
    //assert(game->map->collides(me) == false);
    // check if the actor is on a surface
    bool onsurface = onSurface(game);

    // empty action queue
    actionQueue.unique();
    while (!actionQueue.empty()) {
        Action action = actionQueue.front();
        actionQueue.pop_front();
        
        if (action == RUN_LEFT) {
            faceLeft = true;
            velocity_x -= 0.8;
        } else if (action == RUN_RIGHT) {
            faceLeft = false;
            velocity_x += 0.8;
        } else if (action == JUMP && onsurface) {
            velocity_y -= 2;
        }
    }
    
    if (onsurface) {    
        // apply friction
        if (velocity_x > 0) {
            velocity_x -= 0.1;
            if (velocity_x < 0) {
                velocity_x = 0;
            }
        } else if (velocity_x < 0) {
            velocity_x += 0.1;
            if (velocity_x > 0) {
                velocity_x = 0;
            }
        }
    } else {
        // apply gravity
        velocity_y += 0.1;
    }
    
    // apply velocities
    SDL_Rect horiz_space = {(Sint16) (position_x + velocity_x), (Sint16) position_y, (Uint16) (7 + abs(velocity_x)), 7};
    SDL_Rect vertical_space = {(Sint16) position_x, (Sint16) (position_y + velocity_y), 7, (Uint16) (7 + abs(velocity_y))};

    // move up a hill if needed
    if (game->map->collides(horiz_space)) {
        horiz_space.y--;
        if (game->map->collides(horiz_space)) {
            horiz_space.y--;
            if (game->map->collides(horiz_space)) {
                velocity_x = 0;
            } else {
                position_y -= 2;
                position_x += velocity_x;
            }
        } else {
            position_y -= 1;
            position_x += velocity_x;
        }
    } else {
        position_x += velocity_x;
    }
    
    // check for vertical collisions
    if (game->map->collides(vertical_space)) {
        velocity_y = 0;
    } else {
        position_y += velocity_y;
    }
}

bool Actor::onSurface(shared_ptr<GameData> game) {
    SDL_Rect surface_check = {(Sint16) position_x, (Sint16) position_y, 7, 8};
    return game->map->collides(surface_check);
}

void Actor::render(SDL_Surface* screen, shared_ptr<GameData> game) {
    // TODO: have a better method of figuring out which animation frame to render
    static int frame = 0;
    frame++;
    if (velocity_x < 0) {
        anim->render(screen, position_x - game->camera->x, position_y - game->camera->y, MOVING, (frame / 10) % 2, faceLeft);
    } else if (velocity_x > 0) {
        anim->render(screen, position_x - game->camera->x, position_y - game->camera->y, MOVING, (frame / 10) % 2, faceLeft);
    } else {
        anim->render(screen, position_x - game->camera->x, position_y - game->camera->y, STANDING, 0, faceLeft);
    }
}

bool Actor::collides(shared_ptr<Bitmask> bitmask) {
    SDL_Rect my_area = {(Sint16) position_x, (Sint16) position_y, 7, 8};
    return bitmask->collides(my_area.x, my_area.y, my_area.x + my_area.w, my_area.y + my_area.h);
}

bool Actor::collides(Actor &other) {
	// TODO: check collision
	return false;
}