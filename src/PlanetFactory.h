#ifndef PLANET_FACTORY_H
#define PLANET_FACTORY_H

#include "Map.h"
#include "Environment.h"

class PlanetFactory {
public:
    static std::shared_ptr<MapSlice> generatePlanetMap(int planet_id, int map_location);
    static std::shared_ptr<Environment> generatePlanetEnvironment(int planet_id);
    static std::shared_ptr<Planet> getPlanetInfo(int planet_id);
};

#endif