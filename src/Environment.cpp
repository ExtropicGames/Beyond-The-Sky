#include "Environment.h"

using namespace std;

void Environment::update() {
    time++;
}

void Environment::render(SDL_Surface* screen, shared_ptr<GameData> game) {
    // render sun
    // TODO: calculate sun position from planet's angle & rotation
    filledCircleColor(screen, 50, 50, 10, 0xFFFF00FF);
    // TODO: SDL_gfx's aa functions are f-ed so we should write our own.
    aacircleColor(screen, 50, 50, 10, 0xFFFF00FF);
}