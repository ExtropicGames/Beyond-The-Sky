#include "Map.h"

#include <iostream>

#include "PlanetFactory.h"
#include "Utility/sdlfunc.h"

using namespace std;

Map::Map(int _planet_id, int starting_location) {
    moveTo(_planet_id, starting_location);
}

void Map::renderForeground(SDL_Surface* screen, shared_ptr<GameData> game) {
    // TODO: could optimize this to only blit left and right if they are on the screen
    SDL_Rect where = {0,0,0,0};
    where.x = (Sint16) -game->camera->x - MAP_WIDTH;
    where.y = (Sint16) -game->camera->y;
    SDL_BlitSurface(left->foreground, NULL, screen, &where);
    
    where.x = (Sint16) -game->camera->x;
    where.y = (Sint16) -game->camera->y;
	SDL_BlitSurface(center->foreground, NULL, screen, &where);
    
    where.x = -game->camera->x + MAP_WIDTH;
    where.y = (Sint16) -game->camera->y;
    SDL_BlitSurface(right->foreground, NULL, screen, &where);
}

void Map::renderBackground(SDL_Surface* screen, shared_ptr<GameData> game) {
    // TODO: could optimize this to only blit left and right if they are on the screen
    SDL_Rect where = {0,0,0,0};
    where.x = (Sint16) -game->camera->x - MAP_WIDTH;
    where.y = (Sint16) -game->camera->y;
    SDL_BlitSurface(left->background, NULL, screen, &where);
    
    where.x = (Sint16) -game->camera->x;
    where.y = (Sint16) -game->camera->y;
	SDL_BlitSurface(center->background, NULL, screen, &where);
    
    where.x = -game->camera->x + MAP_WIDTH;
    where.y = (Sint16) -game->camera->y;
    SDL_BlitSurface(right->background, NULL, screen, &where);
}

bool Map::collides(SDL_Rect area) {
    // TODO: could optimize this to only check left and right if player overlaps them
    
    // TODO: there is a glitch in the left and right collision checks that needs a closer look
    /*if (left->collision->collides(area.x - MAP_WIDTH, area.y, area.x + area.w - MAP_WIDTH, area.y + area.h)) {
        cout << "collided with left map" << endl;
        return true;
    }*/
    if (center->collision->collides(area.x, area.y, area.x + area.w, area.y + area.h)) {
        return true;
    }
    /*if (right->collision->collides(area.x + MAP_WIDTH, area.y, area.x + area.w + MAP_WIDTH, area.y + area.h)) {
        cout << "collided with right map" << endl;
        return true;
    }*/
    
    return false;
}

void Map::scrollLeft() {
    location--;
    right = center;
    center = left;
    left = PlanetFactory::generatePlanetMap(planet_id, location-1);
}

void Map::scrollRight() {
    location++;
    left = center;
    center = right;
    right = PlanetFactory::generatePlanetMap(planet_id, location+1);
}

void Map::moveTo(int _planet_id, int _location) {
    planet_id = _planet_id;
    location = _location;
    left = PlanetFactory::generatePlanetMap(planet_id, _location-1);
    center = PlanetFactory::generatePlanetMap(planet_id, _location);
    right = PlanetFactory::generatePlanetMap(planet_id, _location+1);
}