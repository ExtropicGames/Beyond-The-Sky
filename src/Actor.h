#ifndef ACTOR_H
#define ACTOR_H

#include <string>

#ifdef WIN32
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif

#include <list>
#include <Resources/Bitmask.h>

#include "GameData.h"
#include "Resources/Animation.h"

enum Action { 
    NO_ACTION,
	RUN_LEFT, 
	RUN_RIGHT, 
	JUMP,
	FALL 
};

class GameData;

class Actor {
public:
	// public methods
	Actor(std::string name);
    ~Actor();

    void doAction(Action action);
	void update(std::shared_ptr<GameData> game);
	void render(SDL_Surface* screen, std::shared_ptr<GameData> game);
	bool collides(Actor &other);
    bool collides(std::shared_ptr<Bitmask> bitmask);
    
    bool onSurface(std::shared_ptr<GameData> game);

	float position_x = 0;
	float position_y = 0;
private:
    // movement related
    float velocity_x = 0;
    float velocity_y = 0;
    bool faceLeft = true;

    std::list<Action> actionQueue;

    std::shared_ptr<Animation> anim;
    
    // TODO: actors should have their own bitmask and use it for collision checks
};

#endif