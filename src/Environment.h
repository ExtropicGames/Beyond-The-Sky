#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <memory>

#ifdef WIN32
#include <SDL.h>
#include <SDL_gfxPrimitives.h>
#else
#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>
#endif

class GameData;

// TODO: figure out what additional data is needed to simulate orbits
struct Planet {
    int size;
    float angle; // angle of planet from sun
    float rotation; // angle that planet is rotated by
    int perihelion; // closest distance to the sun
    int aphelion; // furthest distance from the sun
};

class Environment {
public:
    void update();
    void render(SDL_Surface* screen, std::shared_ptr<GameData> game);
private:
    long time;
    float planet_angle; // angle of planet from sun
    float planet_rotation; // angle that planet is rotated at
};

#endif