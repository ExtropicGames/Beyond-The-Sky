Compiler   := g++
Kernel     := $(shell uname -s)
Distro     := $(shell uname -r)
Target     := Space
SourceDirs := $(shell ls src/*.cpp)
SourceDirs += $(shell ls src/Resources/*.cpp)
SourceDirs += $(shell ls src/Utility/*.cpp)
Objects    := $(patsubst %.cpp, %.o, $(SourceDirs))
BinPath    := .
Libraries  := -lSDLMain -lSDL -lSDL_ttf -lSDL_image -lSDL_mixer -lSDL_gfx
Includes   := -I./src -I.
Flags      := -g -Wall -O0

# Mac OS X specific settings
ifeq ($(Kernel), Darwin)
Compiler  := clang++
Includes  += -I/usr/local/include/SDL -D_GNU_SOURCE=1 -D_THREAD_SAFE
Libraries += -L/usr/local/lib -Wl,-framework,Cocoa
Flags     += -std=c++11 -stdlib=libc++
endif

# Linux specific settings
ifeq ($(Kernel), Linux)
Flags += -std=c++0x
endif

all: $(Objects)
	$(warning Building...)
	$(Compiler) $(Libraries) $(Objects) $(Sources) $(Includes) -o $(BinPath)/$(Target) $(LinkDirs) $(Flags)

.cpp.o:
	$(Compiler) $(Flags) $(Includes) -c -o $*.o $<

.cxx.o:
	$(Compiler) $(Flags) $(Includes) -c -o $*.o $<

clean:
	rm -f $(Objects)

#multilib handling
ifeq ($(HOSTTYPE), x86_64)
LIBSELECT=64
endif
